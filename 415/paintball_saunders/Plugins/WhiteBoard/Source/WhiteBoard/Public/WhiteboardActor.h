// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WhiteboardActor.generated.h"

UCLASS()
class WHITEBOARD_API AWhiteboardActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWhiteboardActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Mesh
	UPROPERTY()
		UStaticMeshComponent* whiteboardMesh;

	// material interface
	UPROPERTY()
		UMaterialInterface* markerMat;

	// material instance
	UPROPERTY()
		UMaterialInstanceDynamic* whiteboardMarker_DMI;

	// render target
	UPROPERTY()
		UTextureRenderTarget2D* renderTarget;

	// Draw on board
	UFUNCTION(BlueprintCallable)
		void drawOnWhiteboard(FVector2D loc, float drawSize);

	// Clear board
	UFUNCTION(BlueprintCallable)
		void clearWhiteboard();

	bool drawnOn = false;
	bool firstDraw = true;
};
