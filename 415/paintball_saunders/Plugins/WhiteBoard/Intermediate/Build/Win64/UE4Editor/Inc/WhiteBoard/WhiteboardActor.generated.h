// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector2D;
#ifdef WHITEBOARD_WhiteboardActor_generated_h
#error "WhiteboardActor.generated.h already included, missing '#pragma once' in WhiteboardActor.h"
#endif
#define WHITEBOARD_WhiteboardActor_generated_h

#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execclearWhiteboard) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->clearWhiteboard(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execdrawOnWhiteboard) \
	{ \
		P_GET_STRUCT(FVector2D,Z_Param_loc); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_drawSize); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->drawOnWhiteboard(Z_Param_loc,Z_Param_drawSize); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execclearWhiteboard) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->clearWhiteboard(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execdrawOnWhiteboard) \
	{ \
		P_GET_STRUCT(FVector2D,Z_Param_loc); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_drawSize); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->drawOnWhiteboard(Z_Param_loc,Z_Param_drawSize); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWhiteboardActor(); \
	friend WHITEBOARD_API class UClass* Z_Construct_UClass_AWhiteboardActor(); \
public: \
	DECLARE_CLASS(AWhiteboardActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/WhiteBoard"), NO_API) \
	DECLARE_SERIALIZER(AWhiteboardActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAWhiteboardActor(); \
	friend WHITEBOARD_API class UClass* Z_Construct_UClass_AWhiteboardActor(); \
public: \
	DECLARE_CLASS(AWhiteboardActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/WhiteBoard"), NO_API) \
	DECLARE_SERIALIZER(AWhiteboardActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWhiteboardActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWhiteboardActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWhiteboardActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWhiteboardActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWhiteboardActor(AWhiteboardActor&&); \
	NO_API AWhiteboardActor(const AWhiteboardActor&); \
public:


#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWhiteboardActor(AWhiteboardActor&&); \
	NO_API AWhiteboardActor(const AWhiteboardActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWhiteboardActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWhiteboardActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWhiteboardActor)


#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_PRIVATE_PROPERTY_OFFSET
#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_9_PROLOG
#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_RPC_WRAPPERS \
	paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_INCLASS \
	paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Plugins_WhiteBoard_Source_WhiteBoard_Public_WhiteboardActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
