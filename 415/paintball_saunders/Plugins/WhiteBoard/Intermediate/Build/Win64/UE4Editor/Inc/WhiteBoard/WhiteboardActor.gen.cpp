// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/WhiteboardActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWhiteboardActor() {}
// Cross Module References
	WHITEBOARD_API UClass* Z_Construct_UClass_AWhiteboardActor_NoRegister();
	WHITEBOARD_API UClass* Z_Construct_UClass_AWhiteboardActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_WhiteBoard();
	WHITEBOARD_API UFunction* Z_Construct_UFunction_AWhiteboardActor_clearWhiteboard();
	WHITEBOARD_API UFunction* Z_Construct_UFunction_AWhiteboardActor_drawOnWhiteboard();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AWhiteboardActor::StaticRegisterNativesAWhiteboardActor()
	{
		UClass* Class = AWhiteboardActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "clearWhiteboard", (Native)&AWhiteboardActor::execclearWhiteboard },
			{ "drawOnWhiteboard", (Native)&AWhiteboardActor::execdrawOnWhiteboard },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AWhiteboardActor_clearWhiteboard()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/WhiteboardActor.h" },
				{ "ToolTip", "Clear board" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AWhiteboardActor, "clearWhiteboard", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AWhiteboardActor_drawOnWhiteboard()
	{
		struct WhiteboardActor_eventdrawOnWhiteboard_Parms
		{
			FVector2D loc;
			float drawSize;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_drawSize = { UE4CodeGen_Private::EPropertyClass::Float, "drawSize", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(WhiteboardActor_eventdrawOnWhiteboard_Parms, drawSize), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_loc = { UE4CodeGen_Private::EPropertyClass::Struct, "loc", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(WhiteboardActor_eventdrawOnWhiteboard_Parms, loc), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_drawSize,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_loc,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/WhiteboardActor.h" },
				{ "ToolTip", "Draw on board" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AWhiteboardActor, "drawOnWhiteboard", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04820401, sizeof(WhiteboardActor_eventdrawOnWhiteboard_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWhiteboardActor_NoRegister()
	{
		return AWhiteboardActor::StaticClass();
	}
	UClass* Z_Construct_UClass_AWhiteboardActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_WhiteBoard,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AWhiteboardActor_clearWhiteboard, "clearWhiteboard" }, // 1745643113
				{ &Z_Construct_UFunction_AWhiteboardActor_drawOnWhiteboard, "drawOnWhiteboard" }, // 2376088266
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "WhiteboardActor.h" },
				{ "ModuleRelativePath", "Public/WhiteboardActor.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_renderTarget_MetaData[] = {
				{ "ModuleRelativePath", "Public/WhiteboardActor.h" },
				{ "ToolTip", "render target" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_renderTarget = { UE4CodeGen_Private::EPropertyClass::Object, "renderTarget", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000000, 1, nullptr, STRUCT_OFFSET(AWhiteboardActor, renderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(NewProp_renderTarget_MetaData, ARRAY_COUNT(NewProp_renderTarget_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_whiteboardMarker_DMI_MetaData[] = {
				{ "ModuleRelativePath", "Public/WhiteboardActor.h" },
				{ "ToolTip", "material instance" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_whiteboardMarker_DMI = { UE4CodeGen_Private::EPropertyClass::Object, "whiteboardMarker_DMI", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000000, 1, nullptr, STRUCT_OFFSET(AWhiteboardActor, whiteboardMarker_DMI), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(NewProp_whiteboardMarker_DMI_MetaData, ARRAY_COUNT(NewProp_whiteboardMarker_DMI_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_markerMat_MetaData[] = {
				{ "ModuleRelativePath", "Public/WhiteboardActor.h" },
				{ "ToolTip", "material interface" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_markerMat = { UE4CodeGen_Private::EPropertyClass::Object, "markerMat", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000000, 1, nullptr, STRUCT_OFFSET(AWhiteboardActor, markerMat), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(NewProp_markerMat_MetaData, ARRAY_COUNT(NewProp_markerMat_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_whiteboardMesh_MetaData[] = {
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Public/WhiteboardActor.h" },
				{ "ToolTip", "Mesh" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_whiteboardMesh = { UE4CodeGen_Private::EPropertyClass::Object, "whiteboardMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000080008, 1, nullptr, STRUCT_OFFSET(AWhiteboardActor, whiteboardMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_whiteboardMesh_MetaData, ARRAY_COUNT(NewProp_whiteboardMesh_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_renderTarget,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_whiteboardMarker_DMI,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_markerMat,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_whiteboardMesh,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AWhiteboardActor>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AWhiteboardActor::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWhiteboardActor, 2343869895);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWhiteboardActor(Z_Construct_UClass_AWhiteboardActor, &AWhiteboardActor::StaticClass, TEXT("/Script/WhiteBoard"), TEXT("AWhiteboardActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWhiteboardActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
