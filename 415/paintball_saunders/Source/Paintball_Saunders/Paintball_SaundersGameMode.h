// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Paintball_SaundersCharacter.h"
#include "Paintball_SaundersGameMode.generated.h"


UENUM()
enum class EGamePlayState
{
	EPlaying,
	EGameOver,
	EUnknown
};

UCLASS(minimalapi)
class APaintball_SaundersGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintball_SaundersGameMode();

	virtual void BeginPlay() override;
	virtual void Tick(float deltaTime) override;

	APaintball_SaundersCharacter* myCharacter;

	UFUNCTION(BlueprintPure, Category = "Game")
		EGamePlayState GetCurrentState() const;

	void SetCurrentState(EGamePlayState newState);

private:
	EGamePlayState currentState;
	void HandleNewState(EGamePlayState newState);

	bool win;
};



