// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_SaundersCharacter.h"
#include "Paintball_SaundersProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "Kismet/GamePlayStatics.h"
#include "WhiteboardActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// APaintball_SaundersCharacter

APaintball_SaundersCharacter::APaintball_SaundersCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	//FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

#if 0
	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;

	//EndColorBuildup = 0;
	//EndColorBuildupDirection = 1;
	//PixelShaderTopLeftColor = FColor::Green;
	//ComputeShaderSimulationSpeed = 1.0;
	//ComputeShaderBlend = 0.5f;
	//ComputeShaderBlendScalar = 0;
	//TotalElapsedTime = 0;
#endif
}

void APaintball_SaundersCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

#if 0
	//PixelShading = new FPixelShaderUsageExample(
	//	PixelShaderTopLeftColor, 
	//	GetWorld()->Scene->GetFeatureLevel());
	//ComputeShading = new FComputeShaderUsageExample(
	//	ComputeShaderSimulationSpeed, 
	//	1024, 
	//	1024, 
	//	GetWorld()->Scene->GetFeatureLevel());
#endif
}

#if 0
//Do not forget cleanup :) 
//void APaintball_SaundersCharacter::BeginDestroy() {
//	Super::BeginDestroy();
//	if (PixelShading) {
//		delete PixelShading;
//	}
//	if (ComputeShading) {
//		delete ComputeShading;
//	}
//}

//Saving functions
//void APaintball_SaundersCharacter::SavePixelShaderOutput() {
//	PixelShading->Save();
//}
//void APaintball_SaundersCharacter::SaveComputeShaderOutput() {
//	ComputeShading->Save();
//}
//void APaintball_SaundersCharacter::ModifyComputeShaderBlend(float NewScalar) {
//	ComputeShaderBlendScalar = NewScalar;
//}
//void APaintball_SaundersCharacter::Tick(float DeltaSeconds) {
//	Super::Tick(DeltaSeconds);
//	TotalElapsedTime += DeltaSeconds;
//	if (PixelShading) {
//		EndColorBuildup = FMath::Clamp(EndColorBuildup + DeltaSeconds * EndColorBuildupDirection, 0.0f, 1.0f);
//		if (EndColorBuildup >= 1.0 || EndColorBuildup <= 0) {
//			EndColorBuildupDirection *= -1;
//		}
//		FTexture2DRHIRef InputTexture = NULL;
//		if (ComputeShading) {
//			ComputeShading->ExecuteComputeShader(TotalElapsedTime);
//			InputTexture = ComputeShading->GetTexture(); //This is the output texture from the compute shader that we will pass to the pixel shader. 
//		}
//		ComputeShaderBlend = FMath::Clamp(ComputeShaderBlend + ComputeShaderBlendScalar * DeltaSeconds, 0.0f, 1.0f);
//		PixelShading->ExecutePixelShader(RenderTarget, InputTexture, FColor(EndColorBuildup * 255, 0, 0, 255), ComputeShaderBlend);
//	}
//}
#endif

//////////////////////////////////////////////////////////////////////////
// Input

void APaintball_SaundersCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APaintball_SaundersCharacter::OnFire);
	// Release fire so we can clear timer
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &APaintball_SaundersCharacter::OnReleaseFire);
	// Toggle firing of projectiles vs spraypaint
	PlayerInputComponent->BindAction("Toggle", IE_Pressed, this, &APaintball_SaundersCharacter::ToggleFiring);
	PlayerInputComponent->BindAction("smallPaint", IE_Pressed, this, &APaintball_SaundersCharacter::setSmall);
	PlayerInputComponent->BindAction("mediumPaint", IE_Pressed, this, &APaintball_SaundersCharacter::setMedium);
	PlayerInputComponent->BindAction("largePaint", IE_Pressed, this, &APaintball_SaundersCharacter::setLarge);
	PlayerInputComponent->BindAction("extraLargePaint", IE_Pressed, this, &APaintball_SaundersCharacter::setExtraLarge);
	PlayerInputComponent->BindAction("scaleUp", IE_Pressed, this, &APaintball_SaundersCharacter::increaseDraw);
	PlayerInputComponent->BindAction("scaleDown", IE_Pressed, this, &APaintball_SaundersCharacter::decreaseDraw);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &APaintball_SaundersCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &APaintball_SaundersCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APaintball_SaundersCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &APaintball_SaundersCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &APaintball_SaundersCharacter::LookUpAtRate);

#if 0
	//ShaderPluginDemo Specific input mappings  
	//InputComponent->BindAction("SavePixelShaderOutput", IE_Pressed, this, &APaintball_SaundersCharacter::SavePixelShaderOutput);
	//InputComponent->BindAction("SaveComputeShaderOutput", IE_Pressed, this, &APaintball_SaundersCharacter::SaveComputeShaderOutput);
	//InputComponent->BindAxis("ComputeShaderBlend", this, &APaintball_SaundersCharacter::ModifyComputeShaderBlend);
#endif
}

// Toggle ability to fire weapon
void APaintball_SaundersCharacter::ToggleFiring()
{
	if (allowFiring)
	{
		allowFiring = false;
	}
	else
	{
		allowFiring = true;
	}
}

// When firing decide if we should use projectile or spray
void APaintball_SaundersCharacter::OnFire()
{
	// If gun then rapid fire projectiles
	if (allowFiring)
	{
		if (ammoAmt > 0)
		{
			GetWorldTimerManager().SetTimer(MyHandle, this, &APaintball_SaundersCharacter::fireProjectile, 0.15f, true);
		}
	}
	// Else spray paint super rapid fire
	else
	{
		GetWorldTimerManager().SetTimer(MyHandle, this, &APaintball_SaundersCharacter::sprayPaint, 0.0001f, true);
	}
}

// Clear timer when fire released
void APaintball_SaundersCharacter::OnReleaseFire()
{
	GetWorldTimerManager().ClearTimer(MyHandle);
}

// Move of projectile from OnFire so we can wrap in a timer
void APaintball_SaundersCharacter::fireProjectile()
{
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<APaintball_SaundersProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				//MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				//spawn the projectile at the muzzle
				World->SpawnActor<APaintball_SaundersProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
		}
		ammoAmt -= 1;
	}

#if 0
	//FHitResult HitResult;
	//FVector StartLocation = FirstPersonCameraComponent->GetComponentLocation();
	//FRotator Direction = FirstPersonCameraComponent->GetComponentRotation();
	//FVector EndLocation = StartLocation + Direction.Vector() * 10000;

	//FCollisionQueryParams QueryParams;
	//QueryParams.AddIgnoredActor(this);
	//if (GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, QueryParams)) {
	//	TArray <UStaticMeshComponent*> StaticMeshComponents = TArray <UStaticMeshComponent*>();
	//	AActor* HitActor = HitResult.GetActor();
	//	if (NULL != HitActor) {
	//		HitActor->GetComponents <UStaticMeshComponent>(StaticMeshComponents);
	//		for (int32 i = 0; i < StaticMeshComponents.Num(); i++) {
	//			UStaticMeshComponent * CurrentStaticMeshPtr = StaticMeshComponents[i];
	//			CurrentStaticMeshPtr->SetMaterial(0, MaterialToApplyToClickedObject);
	//			UMaterialInstanceDynamic* MID = CurrentStaticMeshPtr->CreateAndSetMaterialInstanceDynamic(0);
	//			UTexture* CastedRenderTarget = Cast <UTexture>(RenderTarget);
	//			MID->SetTextureParameterValue("InputTexture", CastedRenderTarget);
	//		}
	//	}
	//}

#endif

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

// Change brush size routines
// small, medium, large, xl,
// increment, decrement
void APaintball_SaundersCharacter::setSmall()
{
	drawSize = 0.015f;
}

void APaintball_SaundersCharacter::setMedium()
{
	drawSize = 0.025f;
}

void APaintball_SaundersCharacter::setLarge()
{
	drawSize = 0.05f;
}


void APaintball_SaundersCharacter::setExtraLarge()
{
	drawSize = 0.075f;
}

void APaintball_SaundersCharacter::increaseDraw()
{
	// small step up
	drawSize = drawSize + 0.005;
}

void APaintball_SaundersCharacter::decreaseDraw()
{
	// small step down
	drawSize = drawSize - 0.005;
}

// Spray paint ability
void APaintball_SaundersCharacter::sprayPaint()
{
	// Area to store line trace params
	FHitResult OutHit; // hit result
	FVector Start = FP_Gun->GetComponentLocation(); // Location of trace start, player gun
	FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector(); //Forward vector, player direction
	FVector End = ((ForwardVector * 4000.f) + Start); // End trace location forward of player by dist
	FCollisionQueryParams CollisionParams; // Collection of params
	CollisionParams.bTraceComplex = true; // Complex trace
	CollisionParams.AddIgnoredActor(this); // Ignore yourself
	CollisionParams.bReturnFaceIndex = true; // Face index needed for UVs

	// Debug to visualize ray
	//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

	// Line trace to draw on white board if we hit whiteboard actor
	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams))
	{
		// Get the actor hit
		AActor* hitActor = OutHit.GetActor();
		// cast to white board
		AWhiteboardActor* whiteboard = Cast<AWhiteboardActor>(hitActor);
		// If we hit something and that something is a whiteboard
		if (hitActor != NULL && whiteboard)
		{
			FVector2D uvLoc; // UV storage
			UGameplayStatics::FindCollisionUV(OutHit, 0, uvLoc); // Get UV's from object hit
			whiteboard->drawOnWhiteboard(uvLoc, drawSize); // Draw on the board!!!
			// Debug to print the UV data
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, uvLoc.ToString());
		}
	}
}

void APaintball_SaundersCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void APaintball_SaundersCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void APaintball_SaundersCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

void APaintball_SaundersCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void APaintball_SaundersCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void APaintball_SaundersCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APaintball_SaundersCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool APaintball_SaundersCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &APaintball_SaundersCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &APaintball_SaundersCharacter::EndTouch);

		return true;
	}
	return false;
}

// Score getter, setter, increment and decrement

// Set ammo positive or negative
// args:
//		Bool - makes change positive if true and negative if false
//		int - set ammount to change
void APaintball_SaundersCharacter::SetAmmo(bool add, int amt)
{
	if (add)
	{
		ammoAmt += amt;
	}
	else
	{
		ammoAmt -= amt;
	}
}

int APaintball_SaundersCharacter::GetScore()
{
	return score;
}

// Set score by passing in value to change by, only increments
void APaintball_SaundersCharacter::IncrementScoreByValue(int value)
{
	score += value;
}

// Remove one from the score
void APaintball_SaundersCharacter::DecrementScore()
{
	score--;
}

// Get score integer as text for HUD
FText APaintball_SaundersCharacter::GetScoreIntText()
{
	FString Text = FString::FromInt(score);
	FText ScoreText = FText::FromString(Text);
	return ScoreText;
}

// Get ammo integer as text for HUD
FText APaintball_SaundersCharacter::GetAmmoIntText()
{
	FString Text = FString::FromInt(ammoAmt);
	FText ammoText = FText::FromString(Text);
	return ammoText;
}