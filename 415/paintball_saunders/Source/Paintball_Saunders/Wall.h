// Actor to make walls using planes

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Wall.generated.h"

UCLASS()
class PAINTBALL_SAUNDERS_API AWall : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWall();
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void CreateSquare();

private:
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* mesh;
};
