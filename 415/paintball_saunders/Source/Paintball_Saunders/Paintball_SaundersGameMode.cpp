// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_SaundersGameMode.h"
#include "Paintball_SaundersHUD.h"
#include "Paintball_SaundersCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Paintball_SaundersHUD.h"
#include "Engine.h"

APaintball_SaundersGameMode::APaintball_SaundersGameMode()
	: Super()
{
	// Allow tick
	PrimaryActorTick.bCanEverTick = true;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn>PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintball_SaundersHUD::StaticClass();
}


// On begin play actions
// Set game to playing
// Get access to player
void APaintball_SaundersGameMode::BeginPlay()
{
	Super::BeginPlay();

	SetCurrentState(EGamePlayState::EPlaying);

	myCharacter = Cast<APaintball_SaundersCharacter>(
		UGameplayStatics::GetPlayerPawn(this, 0));
}

// On tick check if character has health
// If health is epsilon 0 set the game to over
// If score over 10 set to game over or if score below -3
void APaintball_SaundersGameMode::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	if (myCharacter)
	{
		if (myCharacter->GetScore() >= 100)
		{
			win = true;
			SetCurrentState(EGamePlayState::EGameOver);
		}
		if ((myCharacter->GetScore() < -10) || (myCharacter->ammoAmt < -25))
		{
			win = false;
			SetCurrentState(EGamePlayState::EGameOver);
		}
	}
}


// Get the current state of game, playing, unknown, and game over possible
EGamePlayState APaintball_SaundersGameMode::GetCurrentState() const
{
	return currentState;
}

// Set the state of game play, playing, unknown, and game over are possible
void APaintball_SaundersGameMode::SetCurrentState(EGamePlayState newState)
{
	currentState = newState;
	HandleNewState(currentState);
}

// Actions to take on state change handled in switch case
void APaintball_SaundersGameMode::HandleNewState(EGamePlayState newState)
{
	switch (newState)
	{
		// Playing game state
	case EGamePlayState::EPlaying:
	{
		// continue
	}
	break;
	// Game over state
	// Restart at start of level when this happens
	case EGamePlayState::EGameOver:
	{
		// Display text based on win condition
		if (win)
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1,
					5.f,
					FColor::Green,
					FString::Printf(TEXT("YOU WIN!!!!")));
			}
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1,
					5.f,
					FColor::Red,
					FString::Printf(TEXT("YOU LOSE!!!!")));
			}
		}
		// Open the main level at start position again
		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	}
	break;
	// default state
	default:
	case EGamePlayState::EUnknown:
	{
		// Continue
	}
	break;
	}
}