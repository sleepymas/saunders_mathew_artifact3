/*
	This is used to make walls in the game but is unused
	This is still done through myActor but should change to this file
	To clean up the code
*/

#include "Wall.h"


// Constuctor 
AWall::AWall()
{
	// Add mesh componenets
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>
		(TEXT("GeneratedMesh"));
	RootComponent = mesh;
}

// Use the post functions to create the actor
void AWall::PostActorCreated()
{
	Super::PostActorCreated();
	CreateSquare();
}

void AWall::PostLoad()
{
	Super::PostLoad();
	CreateSquare();
}

void AWall::CreateSquare()
{
	TArray<FVector> verts;
	TArray<int32> tris;
	TArray<FVector> Normals;
	TArray<FLinearColor> Colors;

	// Create the verts of the plane
	// modify here to make the plane larger
	// (always 0, width, height)(x w h)
	// to make a rectangle keep w and h consistent
	// across the adds, they do not need to match
	// unless you want a square then match x w h 
	verts.Add(FVector(0.0f, 0.0f, 0.0f));
	verts.Add(FVector(0.0f, 6300.0f, 0.0f));
	verts.Add(FVector(0.0f, 0.0f, 1500.0f));
	verts.Add(FVector(0.0f, 6300.0f, 1500.0f));

	// Add the tris in proper winding order
	tris.Add(0);
	tris.Add(1);
	tris.Add(2);
	tris.Add(3);
	tris.Add(2);
	tris.Add(1);

	// Create normals now that we have verts and tris
	for (int32 i = 0; i < verts.Num(); i++)
	{
		Normals.Add(FVector(0.0f, 0.0f, 1.0f));
		Colors.Add(FLinearColor::Blue);
	}

	// Create the mesh with all the data we just created
	TArray<FVector2D> UV0;
	TArray<FProcMeshTangent> Tangents;
	mesh->CreateMeshSection_LinearColor(0, verts, tris,
		Normals, UV0, Colors, Tangents, true);
}

