/*
	Hud widgets for the game and crosshair, add to viewport or remove
*/

#include "Paintball_SaundersHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "TimerManager.h"

APaintball_SaundersHUD::APaintball_SaundersHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;

	// Create HUD for health bar and score
	static ConstructorHelpers::FClassFinder<UUserWidget>scoreHud(TEXT
	("/Game/UI/score"));
	scoreWidgetClass = scoreHud.Class;

	// Create HUD for splash screen
	static ConstructorHelpers::FClassFinder<UUserWidget> SplashObj(TEXT
	("/Game/UI/splash"));
	splashWidgetClass = SplashObj.Class;

	// Set a wait bool
	waitOver = false;
}


void APaintball_SaundersHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

void APaintball_SaundersHUD::BeginPlay()
{
	Super::BeginPlay();

	if (splashWidgetClass != nullptr)
	{
		currentWidget = CreateWidget<UUserWidget>(GetWorld(), splashWidgetClass);

		if (currentWidget)
		{
			currentWidget->AddToViewport();
		}
	}

	// Set up a timer to change to game play HUD after 5 seconds
	GetWorldTimerManager().SetTimer(DelayTime,
		this,
		&APaintball_SaundersHUD::SetHud,
		6.2f,
		true,
		0.0f);
}

// Set the hud to game play from the splash screen
void APaintball_SaundersHUD::SetHud()
{
	// If time is up remove splash and replace with health/score widget
	if (waitOver)
	{
		currentWidget->RemoveFromViewport();

		if (scoreWidgetClass != nullptr)
		{
			currentWidget = CreateWidget<UUserWidget>(GetWorld(), scoreWidgetClass);
		}

		if (currentWidget)
		{
			currentWidget->AddToViewport();
		}

		GetWorldTimerManager().ClearTimer(DelayTime);
	}
	else
	{
		waitOver = true;
	}
}