// Actor to use as a target

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"  
#include "Materials/MaterialInstanceDynamic.h"
#include "ProcTarget.generated.h"

UCLASS()
class PAINTBALL_SAUNDERS_API AProcTarget : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AProcTarget();
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius,
		TArray<FVector>& Vertices, TArray<int32>& Triangles,
		TArray<FVector>& Normals, TArray<FVector2D>& UVs,
		TArray<FProcMeshTangent>& Tangents,
		TArray<FColor>& Colors);

	void move(); // move the target
	void changeColor(FLinearColor color); // Color the target
	int numHits = 0; // counter
	bool isAlive = true;

	// Material access
	UPROPERTY(EditAnywhere)
		UMaterial* Material;
	UPROPERTY(EditAnywhere)
		UMaterialInstanceDynamic* MatInst;

private:
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* mesh;
	float getRand();
	void rotate();
};
