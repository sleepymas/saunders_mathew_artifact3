// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Paintball_SaundersProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "WhiteboardActor.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Terrain.h"
#include "MovingTarget.h"
#include "MyActor.h"


// Include cubes for access to them when hit
#include "RunTarget.h"
#include "ProcTarget.h"

APaintball_SaundersProjectile::APaintball_SaundersProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &APaintball_SaundersProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Build up static mesh to attach color material to
	sphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SphereMesh"));
	sphereMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh>sphereMeshAsset(TEXT(
		"StaticMesh'/Game/Geometry/Meshes/Sphere.Sphere'"));
	sphereMesh->SetStaticMesh(sphereMeshAsset.Object);
	sphereMesh->SetRelativeScale3D(FVector(0.38, 0.38, 0.38)); //Scale to make look better

	// Set up dynamic material for mesh
	static ConstructorHelpers::FObjectFinder<UMaterial>projMaterialAsset(
		TEXT("Material'/Game/Materials/projMat-1.projMat-1'"));
	projMaterial = projMaterialAsset.Object;
	projMatInst = UMaterialInstanceDynamic::Create(projMaterial, sphereMesh);

	// Apply the material to the mesh
	sphereMesh->SetMaterial(0, projMatInst);

	// Add particle trail
	trail = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle Created"));
	trail->SetRelativeLocation(FVector::ZeroVector);
	trail->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UParticleSystem>particleAsset(
		TEXT("ParticleSystem'/Game/Particles/ProjParticle.ProjParticle'"));
	trail->SetTemplate(particleAsset.Object);

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 5500.f;
	ProjectileMovement->MaxSpeed = 7500.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// These will be used again in OnHit() to make decal match
	color = FLinearColor(
		UKismetMathLibrary::RandomFloatInRange(0.0, 1.0f),
		UKismetMathLibrary::RandomFloatInRange(0.0, 1.0f),
		UKismetMathLibrary::RandomFloatInRange(0.0, 1.0f),
		1.0f);
	met = UKismetMathLibrary::RandomFloatInRange(0.0, 1.0f);
	rough = UKismetMathLibrary::RandomFloatInRange(0.0, 1.0f);

	// Set color of projectile
	projMatInst->SetVectorParameterValue("color", color);
	projMatInst->SetScalarParameterValue("met", met);
	projMatInst->SetScalarParameterValue("rough", rough);

	trail->SetVectorParameter("color", FVector(color.R, color.G, color.B));

	// Set up on hit Decal material
	static ConstructorHelpers::FObjectFinder<UMaterial> decalMaterial(TEXT(
		"Material'/Game/Materials/splat.splat'"));
	decalMat = decalMaterial.Object;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.5f;	
}

void APaintball_SaundersProjectile::OnHit(UPrimitiveComponent* HitComp, 
	AActor* OtherActor, UPrimitiveComponent* OtherComp, 
	FVector NormalImpulse, const FHitResult& Hit)
{
	// Handle hits to other actors
	if ((OtherActor != NULL) && (OtherActor != this))
	{
		// Casts to check what we hit 
		ARunTarget*			runTarget	= Cast<ARunTarget>(OtherActor);
		AProcTarget*		procTarget	= Cast<AProcTarget>(OtherActor);
		AWhiteboardActor*	whiteboard	= Cast<AWhiteboardActor>(OtherActor);
		ATerrain*			terrain		= Cast<ATerrain>(OtherActor);
		AMovingTarget*		movingTarget = Cast<AMovingTarget>(OtherActor);
		AMyActor*			wall = Cast<AMyActor>(OtherActor);
		
		APaintball_SaundersCharacter* myCharacter = Cast<APaintball_SaundersCharacter>(
			GetWorld()->GetFirstPlayerController()->GetPawn());

		// Make sure we have engine or crash will happen when we print
		if (GEngine)
		{
			// If we hit a wall remove points
			if (wall)
			{
				myCharacter->DecrementScore();
			}
			// If we hit terrain  modify it
			if (terrain)
			{
				terrain->AlterTerrain(Hit.Location);
			}
			// Tell user to hit T to draw a picture and 1-4 change size
			// Check this first since it clears the board
			if (whiteboard)
			{
				if (whiteboard->drawnOn && whiteboard->firstDraw)
				{
					myCharacter->IncrementScoreByValue(25); // raise score
					myCharacter->SetAmmo(true, 20); // rasie ammo amt
					whiteboard->firstDraw = false; // set to one and only draw
				}
				else
				{
					// Remove 9 points
					for (int i = 0; i < 8; i++)
					{
						myCharacter->DecrementScore();
					}
					// Remove 9 ammo
					myCharacter->SetAmmo(false, 9);
				}
				whiteboard->clearWhiteboard();
				/*GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT(
					"Board cleared!!!\nDraw a Picture with T\n1-4 keys for quick brush size!\nUp or Down for small brush size changes!"));*/
			}
			if (movingTarget)
			{
				// If moving target set color and add score
				movingTarget->changeMatProps(false);
				if (movingTarget->isAlive)
				{
					myCharacter->IncrementScoreByValue(5);
				}
				movingTarget->ClearTimer(); // stop moving!
			}
			// If we hit a runtime target say we hit it
			if (runTarget)
			{
				// Say what we hit then set color
				runTarget->changeMatProps(false);
				if (runTarget->isAlive)
				{
					myCharacter->IncrementScoreByValue(3);
				}
				runTarget->isAlive = false;
				/*GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Hit Runtime Target"));*/
			}
			// If we hit a procedural target say we hit it
			// Move the entity and up the hit count of it
			if (procTarget)
			{
				// Since we are going to move the object spawn attached
				decal = UGameplayStatics::SpawnDecalAttached(
					decalMat,
					FVector(UKismetMathLibrary::RandomFloatInRange(20.0f, 40.0f)),
					Hit.GetComponent(),
					FName("Attach Loc"),
					Hit.Location,
					Hit.Normal.Rotation(),
					EAttachLocation::KeepWorldPosition,
					4.0f); // Last param will cause it to fade in 2 seconds
				//Create dynamic material instance
				decalMatInst = decal->CreateDynamicMaterialInstance();
				// hit a target 5 times to lock it
				if (procTarget->numHits <= 4)
				{	
					// Change color to match paint
					procTarget->changeColor(color);
					procTarget->move(); // Move the target
					procTarget->numHits++; // Increment count
				}
				else
				{
					// hit enough times lock in place and set to green
					procTarget->changeColor(FLinearColor::Green);
					if (procTarget->isAlive)
					{
						myCharacter->IncrementScoreByValue(10);
					}
					procTarget->isAlive = false;
				}
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Hit Procedural Target"));
			}
			// If not just spawn a decal
			else
			{
				//Spawn a decal at a location 
				decal = UGameplayStatics::SpawnDecalAtLocation(GetWorld(),
					decalMat,
					FVector(
						UKismetMathLibrary::RandomFloatInRange(20.0f, 40.0f)),
						Hit.Location,
						Hit.Normal.Rotation(),
						0.0f); // could use last arg for life but wanted finer control
				decal->SetFadeOut(2.0f, 8.0f); // so explicitly set fade out starting at 1 second to 3
				//Set dynamic material instance
				decalMatInst = decal->CreateDynamicMaterialInstance();
			}
		}
		//Set params of the decal
		decalMatInst->SetScalarParameterValue("frame", UKismetMathLibrary::RandomIntegerInRange(0, 3));
		decalMatInst->SetVectorParameterValue("color", color);
		decalMatInst->SetScalarParameterValue("met", met);
		decalMatInst->SetScalarParameterValue("rough", rough);
		// Destroy the projectile
		Destroy();
	}
}