// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Spawner.generated.h"

UCLASS()
class PAINTBALL_SAUNDERS_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Call to spawn!
	UFUNCTION(BlueprintCallable)
		void spawnObject();

	// Origin of the spawner object
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector origin;

	// Size of box
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector boxExtent;

	// Location spawn
	UPROPERTY(EditAnywhere)
		FVector loc;

	// What will we spawn, set in UI
	UPROPERTY(EditAnywhere, Category = "Spawn")
		TSubclassOf<AActor>  actorToSpawn;

	// Box for size
	UPROPERTY(EditAnywhere, Category = "Spawn")
		UBoxComponent* boxComp;

	// Num to spawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn")
		int numSpawn;
};
