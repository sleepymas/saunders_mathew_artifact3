// Actor to use as a target

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "MovingTarget.generated.h"

UCLASS()
class PAINTBALL_SAUNDERS_API AMovingTarget : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMovingTarget();
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius,
		TArray<FVector>& Vertices, TArray<int32>& Triangles,
		TArray<FVector>& Normals, TArray<FVector2D>& UVs,
		TArray<FRuntimeMeshTangent>& Tangents,
		TArray<FColor>& Colors);

	// Tick for color change at set time
	virtual void Tick(float DeltaTime);
	// Begin play to set material at start
	virtual void BeginPlay();

	// Function to change material information on the fly
	UFUNCTION(BlueprintCallable)
		void changeMatProps(bool rand);

	// Function to change material information on the fly
	UFUNCTION(BlueprintCallable)
		void ChangePosition();

	UFUNCTION(BlueprintCallable)
		void ClearTimer();

	UPROPERTY(EditAnywhere)
		FTimerHandle MyHandle;

private:
	// Mesh
	UPROPERTY(VisibleAnywhere)
		URuntimeMeshComponent* mesh;

	// Material access
	UPROPERTY(EditAnywhere)
		UMaterial* Material;
	UPROPERTY(EditAnywhere)
		UMaterialInstanceDynamic* MatInst;

	void rotate();
	float getRandForRot();

	// Allow time to tick
	bool bCanTick = true;

	// What the name says, random integer
	int randInt;
public:
	bool isAlive = true;
};

