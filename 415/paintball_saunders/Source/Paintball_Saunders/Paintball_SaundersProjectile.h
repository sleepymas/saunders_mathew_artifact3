// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystem.h"
#include "Paintball_SaundersCharacter.h"
#include "Paintball_SaundersProjectile.generated.h"

UCLASS(config=Game)
class APaintball_SaundersProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	class USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* sphereMesh;

public:
	APaintball_SaundersProjectile();

	// Material access
	UPROPERTY(EditAnywhere)
		UMaterial* projMaterial;
	UPROPERTY(EditAnywhere)
		UMaterialInstanceDynamic* projMatInst;
	UPROPERTY(EditAnywhere)
		UMaterial* decalMat;
	UPROPERTY(EditAnywhere)
		UDecalComponent* decal;
	UPROPERTY(EditAnywhere)
		UMaterialInstanceDynamic* decalMatInst;

	// Material params
	FLinearColor color;
	float met;
	float rough;

	APaintball_SaundersCharacter* myCharacter;

	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* trail;

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }
};

