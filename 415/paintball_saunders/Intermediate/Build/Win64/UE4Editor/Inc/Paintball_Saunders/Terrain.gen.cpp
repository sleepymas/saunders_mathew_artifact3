// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Terrain.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTerrain() {}
// Cross Module References
	PAINTBALL_SAUNDERS_API UClass* Z_Construct_UClass_ATerrain_NoRegister();
	PAINTBALL_SAUNDERS_API UClass* Z_Construct_UClass_ATerrain();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Saunders();
	PAINTBALL_SAUNDERS_API UFunction* Z_Construct_UFunction_ATerrain_AlterTerrain();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
// End Cross Module References
	void ATerrain::StaticRegisterNativesATerrain()
	{
		UClass* Class = ATerrain::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AlterTerrain", (Native)&ATerrain::execAlterTerrain },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_ATerrain_AlterTerrain()
	{
		struct Terrain_eventAlterTerrain_Parms
		{
			FVector loc;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_loc = { UE4CodeGen_Private::EPropertyClass::Struct, "loc", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Terrain_eventAlterTerrain_Parms, loc), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_loc,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Terrain.h" },
				{ "ToolTip", "Alters terrain" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ATerrain, "AlterTerrain", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04820401, sizeof(Terrain_eventAlterTerrain_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATerrain_NoRegister()
	{
		return ATerrain::StaticClass();
	}
	UClass* Z_Construct_UClass_ATerrain()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Saunders,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_ATerrain_AlterTerrain, "AlterTerrain" }, // 2486461563
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Terrain.h" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_depth_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_depth = { UE4CodeGen_Private::EPropertyClass::Struct, "depth", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, depth), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_depth_MetaData, ARRAY_COUNT(NewProp_depth_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_radius_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_radius = { UE4CodeGen_Private::EPropertyClass::Float, "radius", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, radius), METADATA_PARAMS(NewProp_radius_MetaData, ARRAY_COUNT(NewProp_radius_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_recursion_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_recursion = { UE4CodeGen_Private::EPropertyClass::Int, "recursion", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, recursion), METADATA_PARAMS(NewProp_recursion_MetaData, ARRAY_COUNT(NewProp_recursion_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_vertColors_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_vertColors = { UE4CodeGen_Private::EPropertyClass::Array, "vertColors", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, vertColors), METADATA_PARAMS(NewProp_vertColors_MetaData, ARRAY_COUNT(NewProp_vertColors_MetaData)) };
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_vertColors_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "vertColors", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_trisNew_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_trisNew = { UE4CodeGen_Private::EPropertyClass::Array, "trisNew", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, trisNew), METADATA_PARAMS(NewProp_trisNew_MetaData, ARRAY_COUNT(NewProp_trisNew_MetaData)) };
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_trisNew_Inner = { UE4CodeGen_Private::EPropertyClass::Int, "trisNew", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_tris_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_tris = { UE4CodeGen_Private::EPropertyClass::Array, "tris", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, tris), METADATA_PARAMS(NewProp_tris_MetaData, ARRAY_COUNT(NewProp_tris_MetaData)) };
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_tris_Inner = { UE4CodeGen_Private::EPropertyClass::Int, "tris", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_vertsNew_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_vertsNew = { UE4CodeGen_Private::EPropertyClass::Array, "vertsNew", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, vertsNew), METADATA_PARAMS(NewProp_vertsNew_MetaData, ARRAY_COUNT(NewProp_vertsNew_MetaData)) };
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_vertsNew_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "vertsNew", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_normals_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_normals = { UE4CodeGen_Private::EPropertyClass::Array, "normals", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, normals), METADATA_PARAMS(NewProp_normals_MetaData, ARRAY_COUNT(NewProp_normals_MetaData)) };
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_normals_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "normals", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_verts_MetaData[] = {
				{ "Category", "Defaults" },
				{ "ModuleRelativePath", "Terrain.h" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_verts = { UE4CodeGen_Private::EPropertyClass::Array, "verts", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ATerrain, verts), METADATA_PARAMS(NewProp_verts_MetaData, ARRAY_COUNT(NewProp_verts_MetaData)) };
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_verts_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "verts", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TerrainMesh_MetaData[] = {
				{ "Category", "Defaults" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Terrain.h" },
				{ "ToolTip", "Data for the terrain mesh so we can store and modify" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TerrainMesh = { UE4CodeGen_Private::EPropertyClass::Object, "TerrainMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(ATerrain, TerrainMesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_TerrainMesh_MetaData, ARRAY_COUNT(NewProp_TerrainMesh_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_depth,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_radius,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_recursion,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_vertColors,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_vertColors_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_trisNew,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_trisNew_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_tris,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_tris_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_vertsNew,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_vertsNew_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_normals,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_normals_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_verts,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_verts_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TerrainMesh,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ATerrain>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ATerrain::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATerrain, 3228565959);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATerrain(Z_Construct_UClass_ATerrain, &ATerrain::StaticClass, TEXT("/Script/Paintball_Saunders"), TEXT("ATerrain"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATerrain);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
