// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_SAUNDERS_RunTarget_generated_h
#error "RunTarget.generated.h already included, missing '#pragma once' in RunTarget.h"
#endif
#define PAINTBALL_SAUNDERS_RunTarget_generated_h

#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execchangeMatProps) \
	{ \
		P_GET_UBOOL(Z_Param_rand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->changeMatProps(Z_Param_rand); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execchangeMatProps) \
	{ \
		P_GET_UBOOL(Z_Param_rand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->changeMatProps(Z_Param_rand); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunTarget(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_ARunTarget(); \
public: \
	DECLARE_CLASS(ARunTarget, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(ARunTarget) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_INCLASS \
private: \
	static void StaticRegisterNativesARunTarget(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_ARunTarget(); \
public: \
	DECLARE_CLASS(ARunTarget, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(ARunTarget) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunTarget(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunTarget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunTarget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunTarget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunTarget(ARunTarget&&); \
	NO_API ARunTarget(const ARunTarget&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunTarget(ARunTarget&&); \
	NO_API ARunTarget(const ARunTarget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunTarget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunTarget); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunTarget)


#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(ARunTarget, mesh); } \
	FORCEINLINE static uint32 __PPO__Material() { return STRUCT_OFFSET(ARunTarget, Material); } \
	FORCEINLINE static uint32 __PPO__MatInst() { return STRUCT_OFFSET(ARunTarget, MatInst); }


#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_11_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_RunTarget_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_RunTarget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
