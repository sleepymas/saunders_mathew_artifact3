// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_SAUNDERS_ProcTarget_generated_h
#error "ProcTarget.generated.h already included, missing '#pragma once' in ProcTarget.h"
#endif
#define PAINTBALL_SAUNDERS_ProcTarget_generated_h

#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_RPC_WRAPPERS
#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProcTarget(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_AProcTarget(); \
public: \
	DECLARE_CLASS(AProcTarget, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(AProcTarget) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAProcTarget(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_AProcTarget(); \
public: \
	DECLARE_CLASS(AProcTarget, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(AProcTarget) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProcTarget(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProcTarget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProcTarget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProcTarget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProcTarget(AProcTarget&&); \
	NO_API AProcTarget(const AProcTarget&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProcTarget(AProcTarget&&); \
	NO_API AProcTarget(const AProcTarget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProcTarget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProcTarget); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProcTarget)


#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(AProcTarget, mesh); }


#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_11_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_ProcTarget_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_ProcTarget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
