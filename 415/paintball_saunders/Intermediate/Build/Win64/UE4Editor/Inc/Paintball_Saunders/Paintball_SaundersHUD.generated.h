// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_SAUNDERS_Paintball_SaundersHUD_generated_h
#error "Paintball_SaundersHUD.generated.h already included, missing '#pragma once' in Paintball_SaundersHUD.h"
#endif
#define PAINTBALL_SAUNDERS_Paintball_SaundersHUD_generated_h

#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetHud) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetHud(); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetHud) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetHud(); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_SaundersHUD(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_APaintball_SaundersHUD(); \
public: \
	DECLARE_CLASS(APaintball_SaundersHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(APaintball_SaundersHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_SaundersHUD(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_APaintball_SaundersHUD(); \
public: \
	DECLARE_CLASS(APaintball_SaundersHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(APaintball_SaundersHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_SaundersHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_SaundersHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_SaundersHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_SaundersHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_SaundersHUD(APaintball_SaundersHUD&&); \
	NO_API APaintball_SaundersHUD(const APaintball_SaundersHUD&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_SaundersHUD(APaintball_SaundersHUD&&); \
	NO_API APaintball_SaundersHUD(const APaintball_SaundersHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_SaundersHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_SaundersHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_SaundersHUD)


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__scoreWidgetClass() { return STRUCT_OFFSET(APaintball_SaundersHUD, scoreWidgetClass); } \
	FORCEINLINE static uint32 __PPO__splashWidgetClass() { return STRUCT_OFFSET(APaintball_SaundersHUD, splashWidgetClass); } \
	FORCEINLINE static uint32 __PPO__currentWidget() { return STRUCT_OFFSET(APaintball_SaundersHUD, currentWidget); }


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_10_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
