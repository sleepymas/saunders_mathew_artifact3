// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_SAUNDERS_Wall_generated_h
#error "Wall.generated.h already included, missing '#pragma once' in Wall.h"
#endif
#define PAINTBALL_SAUNDERS_Wall_generated_h

#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_RPC_WRAPPERS
#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWall(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_AWall(); \
public: \
	DECLARE_CLASS(AWall, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(AWall) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAWall(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_AWall(); \
public: \
	DECLARE_CLASS(AWall, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(AWall) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWall(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWall) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWall); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWall); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWall(AWall&&); \
	NO_API AWall(const AWall&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWall(AWall&&); \
	NO_API AWall(const AWall&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWall); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWall); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWall)


#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(AWall, mesh); }


#define paintball_saunders_Source_Paintball_Saunders_Wall_h_10_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Wall_h_13_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Wall_h_13_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_Wall_h_13_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_Wall_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_Wall_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Wall_h_13_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Wall_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Wall_h_13_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Wall_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_Wall_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
