// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef PAINTBALL_SAUNDERS_Paintball_SaundersProjectile_generated_h
#error "Paintball_SaundersProjectile.generated.h already included, missing '#pragma once' in Paintball_SaundersProjectile.h"
#endif
#define PAINTBALL_SAUNDERS_Paintball_SaundersProjectile_generated_h

#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_SaundersProjectile(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_APaintball_SaundersProjectile(); \
public: \
	DECLARE_CLASS(APaintball_SaundersProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(APaintball_SaundersProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_SaundersProjectile(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_APaintball_SaundersProjectile(); \
public: \
	DECLARE_CLASS(APaintball_SaundersProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(APaintball_SaundersProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_SaundersProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_SaundersProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_SaundersProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_SaundersProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_SaundersProjectile(APaintball_SaundersProjectile&&); \
	NO_API APaintball_SaundersProjectile(const APaintball_SaundersProjectile&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_SaundersProjectile(APaintball_SaundersProjectile&&); \
	NO_API APaintball_SaundersProjectile(const APaintball_SaundersProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_SaundersProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_SaundersProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_SaundersProjectile)


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(APaintball_SaundersProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(APaintball_SaundersProjectile, ProjectileMovement); } \
	FORCEINLINE static uint32 __PPO__sphereMesh() { return STRUCT_OFFSET(APaintball_SaundersProjectile, sphereMesh); }


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_13_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
