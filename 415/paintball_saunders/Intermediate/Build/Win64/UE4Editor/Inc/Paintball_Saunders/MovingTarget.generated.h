// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_SAUNDERS_MovingTarget_generated_h
#error "MovingTarget.generated.h already included, missing '#pragma once' in MovingTarget.h"
#endif
#define PAINTBALL_SAUNDERS_MovingTarget_generated_h

#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execClearTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ClearTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangePosition) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ChangePosition(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execchangeMatProps) \
	{ \
		P_GET_UBOOL(Z_Param_rand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->changeMatProps(Z_Param_rand); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execClearTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ClearTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangePosition) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ChangePosition(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execchangeMatProps) \
	{ \
		P_GET_UBOOL(Z_Param_rand); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->changeMatProps(Z_Param_rand); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMovingTarget(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_AMovingTarget(); \
public: \
	DECLARE_CLASS(AMovingTarget, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(AMovingTarget) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMovingTarget(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_AMovingTarget(); \
public: \
	DECLARE_CLASS(AMovingTarget, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(AMovingTarget) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMovingTarget(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMovingTarget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMovingTarget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovingTarget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMovingTarget(AMovingTarget&&); \
	NO_API AMovingTarget(const AMovingTarget&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMovingTarget(AMovingTarget&&); \
	NO_API AMovingTarget(const AMovingTarget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMovingTarget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMovingTarget); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMovingTarget)


#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(AMovingTarget, mesh); } \
	FORCEINLINE static uint32 __PPO__Material() { return STRUCT_OFFSET(AMovingTarget, Material); } \
	FORCEINLINE static uint32 __PPO__MatInst() { return STRUCT_OFFSET(AMovingTarget, MatInst); }


#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_11_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_MovingTarget_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_MovingTarget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
