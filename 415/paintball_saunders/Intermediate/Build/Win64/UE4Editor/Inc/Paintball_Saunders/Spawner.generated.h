// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_SAUNDERS_Spawner_generated_h
#error "Spawner.generated.h already included, missing '#pragma once' in Spawner.h"
#endif
#define PAINTBALL_SAUNDERS_Spawner_generated_h

#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execspawnObject) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->spawnObject(); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execspawnObject) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->spawnObject(); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_ASpawner(); \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(ASpawner) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASpawner(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_ASpawner(); \
public: \
	DECLARE_CLASS(ASpawner, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(ASpawner) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawner(ASpawner&&); \
	NO_API ASpawner(const ASpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawner)


#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_PRIVATE_PROPERTY_OFFSET
#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_10_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Spawner_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_Spawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
