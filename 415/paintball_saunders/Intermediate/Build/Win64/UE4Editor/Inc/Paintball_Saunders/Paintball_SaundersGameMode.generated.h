// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGamePlayState : int32;
#ifdef PAINTBALL_SAUNDERS_Paintball_SaundersGameMode_generated_h
#error "Paintball_SaundersGameMode.generated.h already included, missing '#pragma once' in Paintball_SaundersGameMode.h"
#endif
#define PAINTBALL_SAUNDERS_Paintball_SaundersGameMode_generated_h

#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=this->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=this->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_SaundersGameMode(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_APaintball_SaundersGameMode(); \
public: \
	DECLARE_CLASS(APaintball_SaundersGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Paintball_Saunders"), PAINTBALL_SAUNDERS_API) \
	DECLARE_SERIALIZER(APaintball_SaundersGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_SaundersGameMode(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_APaintball_SaundersGameMode(); \
public: \
	DECLARE_CLASS(APaintball_SaundersGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Paintball_Saunders"), PAINTBALL_SAUNDERS_API) \
	DECLARE_SERIALIZER(APaintball_SaundersGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PAINTBALL_SAUNDERS_API APaintball_SaundersGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_SaundersGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINTBALL_SAUNDERS_API, APaintball_SaundersGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_SaundersGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINTBALL_SAUNDERS_API APaintball_SaundersGameMode(APaintball_SaundersGameMode&&); \
	PAINTBALL_SAUNDERS_API APaintball_SaundersGameMode(const APaintball_SaundersGameMode&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINTBALL_SAUNDERS_API APaintball_SaundersGameMode(APaintball_SaundersGameMode&&); \
	PAINTBALL_SAUNDERS_API APaintball_SaundersGameMode(const APaintball_SaundersGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINTBALL_SAUNDERS_API, APaintball_SaundersGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_SaundersGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_SaundersGameMode)


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_PRIVATE_PROPERTY_OFFSET
#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_19_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersGameMode_h


#define FOREACH_ENUM_EGAMEPLAYSTATE(op) \
	op(EGamePlayState::EPlaying) \
	op(EGamePlayState::EGameOver) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
