// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PAINTBALL_SAUNDERS_Ammo_generated_h
#error "Ammo.generated.h already included, missing '#pragma once' in Ammo.h"
#endif
#define PAINTBALL_SAUNDERS_Ammo_generated_h

#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnComponentBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnComponentBeginOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnComponentBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnComponentBeginOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAmmo(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_AAmmo(); \
public: \
	DECLARE_CLASS(AAmmo, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(AAmmo) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAmmo(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_AAmmo(); \
public: \
	DECLARE_CLASS(AAmmo, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(AAmmo) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAmmo(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAmmo) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAmmo); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAmmo); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAmmo(AAmmo&&); \
	NO_API AAmmo(const AAmmo&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAmmo(AAmmo&&); \
	NO_API AAmmo(const AAmmo&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAmmo); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAmmo); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAmmo)


#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(AAmmo, mesh); }


#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_12_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Ammo_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_Ammo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
