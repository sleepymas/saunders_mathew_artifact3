// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Paintball_SaundersHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintball_SaundersHUD() {}
// Cross Module References
	PAINTBALL_SAUNDERS_API UClass* Z_Construct_UClass_APaintball_SaundersHUD_NoRegister();
	PAINTBALL_SAUNDERS_API UClass* Z_Construct_UClass_APaintball_SaundersHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Paintball_Saunders();
	PAINTBALL_SAUNDERS_API UFunction* Z_Construct_UFunction_APaintball_SaundersHUD_SetHud();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	void APaintball_SaundersHUD::StaticRegisterNativesAPaintball_SaundersHUD()
	{
		UClass* Class = APaintball_SaundersHUD::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetHud", (Native)&APaintball_SaundersHUD::execSetHud },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_APaintball_SaundersHUD_SetHud()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Paintball_SaundersHUD.h" },
				{ "ToolTip", "Change to gameplay hud" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APaintball_SaundersHUD, "SetHud", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APaintball_SaundersHUD_NoRegister()
	{
		return APaintball_SaundersHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_APaintball_SaundersHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Saunders,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_APaintball_SaundersHUD_SetHud, "SetHud" }, // 1409098309
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "Paintball_SaundersHUD.h" },
				{ "ModuleRelativePath", "Paintball_SaundersHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentWidget_MetaData[] = {
				{ "Category", "Widget" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Paintball_SaundersHUD.h" },
				{ "ToolTip", "Widget current displayed" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentWidget = { UE4CodeGen_Private::EPropertyClass::Object, "currentWidget", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000080009, 1, nullptr, STRUCT_OFFSET(APaintball_SaundersHUD, currentWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(NewProp_currentWidget_MetaData, ARRAY_COUNT(NewProp_currentWidget_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_splashWidgetClass_MetaData[] = {
				{ "Category", "Splash" },
				{ "ModuleRelativePath", "Paintball_SaundersHUD.h" },
				{ "ToolTip", "Splash screen" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_splashWidgetClass = { UE4CodeGen_Private::EPropertyClass::Class, "splashWidgetClass", RF_Public|RF_Transient|RF_MarkAsNative, 0x0044000000000001, 1, nullptr, STRUCT_OFFSET(APaintball_SaundersHUD, splashWidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_splashWidgetClass_MetaData, ARRAY_COUNT(NewProp_splashWidgetClass_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_scoreWidgetClass_MetaData[] = {
				{ "Category", "Score" },
				{ "ModuleRelativePath", "Paintball_SaundersHUD.h" },
				{ "ToolTip", "Score widget" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_scoreWidgetClass = { UE4CodeGen_Private::EPropertyClass::Class, "scoreWidgetClass", RF_Public|RF_Transient|RF_MarkAsNative, 0x0044000000000001, 1, nullptr, STRUCT_OFFSET(APaintball_SaundersHUD, scoreWidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_scoreWidgetClass_MetaData, ARRAY_COUNT(NewProp_scoreWidgetClass_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_currentWidget,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_splashWidgetClass,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_scoreWidgetClass,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APaintball_SaundersHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APaintball_SaundersHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintball_SaundersHUD, 3330281149);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintball_SaundersHUD(Z_Construct_UClass_APaintball_SaundersHUD, &APaintball_SaundersHUD::StaticClass, TEXT("/Script/Paintball_Saunders"), TEXT("APaintball_SaundersHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintball_SaundersHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
