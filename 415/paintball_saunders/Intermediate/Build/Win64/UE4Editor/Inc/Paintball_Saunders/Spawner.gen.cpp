// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Spawner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpawner() {}
// Cross Module References
	PAINTBALL_SAUNDERS_API UClass* Z_Construct_UClass_ASpawner_NoRegister();
	PAINTBALL_SAUNDERS_API UClass* Z_Construct_UClass_ASpawner();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Saunders();
	PAINTBALL_SAUNDERS_API UFunction* Z_Construct_UFunction_ASpawner_spawnObject();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ASpawner::StaticRegisterNativesASpawner()
	{
		UClass* Class = ASpawner::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "spawnObject", (Native)&ASpawner::execspawnObject },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_ASpawner_spawnObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Spawner.h" },
				{ "ToolTip", "Call to spawn!" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ASpawner, "spawnObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASpawner_NoRegister()
	{
		return ASpawner::StaticClass();
	}
	UClass* Z_Construct_UClass_ASpawner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Saunders,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_ASpawner_spawnObject, "spawnObject" }, // 3329062627
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Spawner.h" },
				{ "ModuleRelativePath", "Spawner.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_numSpawn_MetaData[] = {
				{ "Category", "Spawn" },
				{ "ModuleRelativePath", "Spawner.h" },
				{ "ToolTip", "Num to spawn" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_numSpawn = { UE4CodeGen_Private::EPropertyClass::Int, "numSpawn", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ASpawner, numSpawn), METADATA_PARAMS(NewProp_numSpawn_MetaData, ARRAY_COUNT(NewProp_numSpawn_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_boxComp_MetaData[] = {
				{ "Category", "Spawn" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Spawner.h" },
				{ "ToolTip", "Box for size" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_boxComp = { UE4CodeGen_Private::EPropertyClass::Object, "boxComp", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000080009, 1, nullptr, STRUCT_OFFSET(ASpawner, boxComp), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(NewProp_boxComp_MetaData, ARRAY_COUNT(NewProp_boxComp_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_actorToSpawn_MetaData[] = {
				{ "Category", "Spawn" },
				{ "ModuleRelativePath", "Spawner.h" },
				{ "ToolTip", "What will we spawn, set in UI" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_actorToSpawn = { UE4CodeGen_Private::EPropertyClass::Class, "actorToSpawn", RF_Public|RF_Transient|RF_MarkAsNative, 0x0014000000000001, 1, nullptr, STRUCT_OFFSET(ASpawner, actorToSpawn), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_actorToSpawn_MetaData, ARRAY_COUNT(NewProp_actorToSpawn_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_loc_MetaData[] = {
				{ "Category", "Spawner" },
				{ "ModuleRelativePath", "Spawner.h" },
				{ "ToolTip", "Location spawn" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_loc = { UE4CodeGen_Private::EPropertyClass::Struct, "loc", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ASpawner, loc), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_loc_MetaData, ARRAY_COUNT(NewProp_loc_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_boxExtent_MetaData[] = {
				{ "Category", "Spawner" },
				{ "ModuleRelativePath", "Spawner.h" },
				{ "ToolTip", "Size of box" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_boxExtent = { UE4CodeGen_Private::EPropertyClass::Struct, "boxExtent", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ASpawner, boxExtent), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_boxExtent_MetaData, ARRAY_COUNT(NewProp_boxExtent_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_origin_MetaData[] = {
				{ "Category", "Spawner" },
				{ "ModuleRelativePath", "Spawner.h" },
				{ "ToolTip", "Origin of the spawner object" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_origin = { UE4CodeGen_Private::EPropertyClass::Struct, "origin", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ASpawner, origin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_origin_MetaData, ARRAY_COUNT(NewProp_origin_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_numSpawn,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_boxComp,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_actorToSpawn,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_loc,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_boxExtent,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_origin,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ASpawner>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ASpawner::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpawner, 1347536601);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpawner(Z_Construct_UClass_ASpawner, &ASpawner::StaticClass, TEXT("/Script/Paintball_Saunders"), TEXT("ASpawner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpawner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
