// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "MovingTarget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovingTarget() {}
// Cross Module References
	PAINTBALL_SAUNDERS_API UClass* Z_Construct_UClass_AMovingTarget_NoRegister();
	PAINTBALL_SAUNDERS_API UClass* Z_Construct_UClass_AMovingTarget();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Paintball_Saunders();
	PAINTBALL_SAUNDERS_API UFunction* Z_Construct_UFunction_AMovingTarget_changeMatProps();
	PAINTBALL_SAUNDERS_API UFunction* Z_Construct_UFunction_AMovingTarget_ChangePosition();
	PAINTBALL_SAUNDERS_API UFunction* Z_Construct_UFunction_AMovingTarget_ClearTimer();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	RUNTIMEMESHCOMPONENT_API UClass* Z_Construct_UClass_URuntimeMeshComponent_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
// End Cross Module References
	void AMovingTarget::StaticRegisterNativesAMovingTarget()
	{
		UClass* Class = AMovingTarget::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "changeMatProps", (Native)&AMovingTarget::execchangeMatProps },
			{ "ChangePosition", (Native)&AMovingTarget::execChangePosition },
			{ "ClearTimer", (Native)&AMovingTarget::execClearTimer },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AMovingTarget_changeMatProps()
	{
		struct MovingTarget_eventchangeMatProps_Parms
		{
			bool rand;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_rand_SetBit = [](void* Obj){ ((MovingTarget_eventchangeMatProps_Parms*)Obj)->rand = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_rand = { UE4CodeGen_Private::EPropertyClass::Bool, "rand", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(MovingTarget_eventchangeMatProps_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_rand_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_rand,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "MovingTarget.h" },
				{ "ToolTip", "Function to change material information on the fly" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AMovingTarget, "changeMatProps", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(MovingTarget_eventchangeMatProps_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AMovingTarget_ChangePosition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "MovingTarget.h" },
				{ "ToolTip", "Function to change material information on the fly" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AMovingTarget, "ChangePosition", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AMovingTarget_ClearTimer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "MovingTarget.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AMovingTarget, "ClearTimer", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMovingTarget_NoRegister()
	{
		return AMovingTarget::StaticClass();
	}
	UClass* Z_Construct_UClass_AMovingTarget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Paintball_Saunders,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AMovingTarget_changeMatProps, "changeMatProps" }, // 1741625898
				{ &Z_Construct_UFunction_AMovingTarget_ChangePosition, "ChangePosition" }, // 536810032
				{ &Z_Construct_UFunction_AMovingTarget_ClearTimer, "ClearTimer" }, // 2866190258
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "MovingTarget.h" },
				{ "ModuleRelativePath", "MovingTarget.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatInst_MetaData[] = {
				{ "Category", "MovingTarget" },
				{ "ModuleRelativePath", "MovingTarget.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MatInst = { UE4CodeGen_Private::EPropertyClass::Object, "MatInst", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000000001, 1, nullptr, STRUCT_OFFSET(AMovingTarget, MatInst), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(NewProp_MatInst_MetaData, ARRAY_COUNT(NewProp_MatInst_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[] = {
				{ "Category", "MovingTarget" },
				{ "ModuleRelativePath", "MovingTarget.h" },
				{ "ToolTip", "Material access" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material = { UE4CodeGen_Private::EPropertyClass::Object, "Material", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000000001, 1, nullptr, STRUCT_OFFSET(AMovingTarget, Material), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(NewProp_Material_MetaData, ARRAY_COUNT(NewProp_Material_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mesh_MetaData[] = {
				{ "Category", "MovingTarget" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "MovingTarget.h" },
				{ "ToolTip", "Mesh" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_mesh = { UE4CodeGen_Private::EPropertyClass::Object, "mesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a0009, 1, nullptr, STRUCT_OFFSET(AMovingTarget, mesh), Z_Construct_UClass_URuntimeMeshComponent_NoRegister, METADATA_PARAMS(NewProp_mesh_MetaData, ARRAY_COUNT(NewProp_mesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyHandle_MetaData[] = {
				{ "Category", "MovingTarget" },
				{ "ModuleRelativePath", "MovingTarget.h" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_MyHandle = { UE4CodeGen_Private::EPropertyClass::Struct, "MyHandle", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(AMovingTarget, MyHandle), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(NewProp_MyHandle_MetaData, ARRAY_COUNT(NewProp_MyHandle_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_MatInst,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Material,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_mesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_MyHandle,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AMovingTarget>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AMovingTarget::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMovingTarget, 1637531647);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMovingTarget(Z_Construct_UClass_AMovingTarget, &AMovingTarget::StaticClass, TEXT("/Script/Paintball_Saunders"), TEXT("AMovingTarget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMovingTarget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
