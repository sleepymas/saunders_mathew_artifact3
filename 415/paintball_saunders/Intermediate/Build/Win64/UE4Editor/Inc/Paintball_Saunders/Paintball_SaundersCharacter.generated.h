// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBALL_SAUNDERS_Paintball_SaundersCharacter_generated_h
#error "Paintball_SaundersCharacter.generated.h already included, missing '#pragma once' in Paintball_SaundersCharacter.h"
#endif
#define PAINTBALL_SAUNDERS_Paintball_SaundersCharacter_generated_h

#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAmmoIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=this->GetAmmoIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetScoreIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=this->GetScoreIntText(); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAmmoIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=this->GetAmmoIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetScoreIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=this->GetScoreIntText(); \
		P_NATIVE_END; \
	}


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintball_SaundersCharacter(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_APaintball_SaundersCharacter(); \
public: \
	DECLARE_CLASS(APaintball_SaundersCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(APaintball_SaundersCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAPaintball_SaundersCharacter(); \
	friend PAINTBALL_SAUNDERS_API class UClass* Z_Construct_UClass_APaintball_SaundersCharacter(); \
public: \
	DECLARE_CLASS(APaintball_SaundersCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Paintball_Saunders"), NO_API) \
	DECLARE_SERIALIZER(APaintball_SaundersCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintball_SaundersCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintball_SaundersCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_SaundersCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_SaundersCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_SaundersCharacter(APaintball_SaundersCharacter&&); \
	NO_API APaintball_SaundersCharacter(const APaintball_SaundersCharacter&); \
public:


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintball_SaundersCharacter(APaintball_SaundersCharacter&&); \
	NO_API APaintball_SaundersCharacter(const APaintball_SaundersCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintball_SaundersCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintball_SaundersCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintball_SaundersCharacter)


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(APaintball_SaundersCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(APaintball_SaundersCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(APaintball_SaundersCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(APaintball_SaundersCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(APaintball_SaundersCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(APaintball_SaundersCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(APaintball_SaundersCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(APaintball_SaundersCharacter, L_MotionController); }


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_15_PROLOG
#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_RPC_WRAPPERS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_INCLASS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_INCLASS_NO_PURE_DECLS \
	paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID paintball_saunders_Source_Paintball_Saunders_Paintball_SaundersCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
